# Interface

Part of [Elegant Roots](https://gitlab.com/elegant-roots).
It's made for providing data about **french** cadaster. Searching by zipcode, section and plot.
Area coordinates will be provided to help people.

## Features

- Cadaster database
- Searching by zipcode, section and plot.
- Getting area coordinates to calculate surface.

## Getting Started

### Prerequisites

List of things you need to install the software and how to install them.

- JRE or JDK 21 install on machine
- Docker + Docker Socket access
- Console prompt access

### Installation

Step-by-step guide to get your development environment set up.

1. Clone this repository.
2. cd to inside

## Usage

Instructions on how to use the project.

```bash
./gradlew run
```

This will start the project with docker container for infrastructure requirements.

## Documentation

- [User Guide](https://docs.micronaut.io/4.5.0/guide/index.html)
- [API Reference](https://docs.micronaut.io/4.5.0/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/4.5.0/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)
- [Micronaut Gradle Plugin documentation](https://micronaut-projects.github.io/micronaut-gradle-plugin/latest/)
- [GraalVM Gradle Plugin documentation](https://graalvm.github.io/native-build-tools/latest/gradle-plugin.html)
- [Shadow Gradle Plugin](https://plugins.gradle.org/plugin/com.github.johnrengelman.shadow)
- [Micronaut OpenAPI Support documentation](https://micronaut-projects.github.io/micronaut-openapi/latest/guide/index.html)
- [https://www.openapis.org](https://www.openapis.org)
- [Micronaut AOT documentation](https://micronaut-projects.github.io/micronaut-aot/latest/guide/)
- [Micronaut R2DBC documentation](https://micronaut-projects.github.io/micronaut-r2dbc/latest/guide/)
- [https://r2dbc.io](https://r2dbc.io)
- [Micronaut Serialization Jackson Core documentation](https://micronaut-projects.github.io/micronaut-serialization/latest/guide/)
- [Micronaut Test Resources documentation](https://micronaut-projects.github.io/micronaut-test-resources/latest/guide/)
- [Micronaut Management documentation](https://docs.micronaut.io/latest/guide/index.html#management)
- [Micronaut Hikari JDBC Connection Pool documentation](https://micronaut-projects.github.io/micronaut-sql/latest/guide/index.html#jdbc)
- [Micronaut Liquibase Database Migration documentation](https://micronaut-projects.github.io/micronaut-liquibase/latest/guide/index.html)
- [https://www.liquibase.org/](https://www.liquibase.org/)
- [Micronaut Data R2DBC documentation](https://micronaut-projects.github.io/micronaut-data/latest/guide/#dbc)

## License

This project is licensed under the GNU General Public License v3.0 License - see the [LICENSE.md](LICENSE) file for details.

## Contact

Orion Beauny-Sugot - support@elegant-roots.com

Project Link: https://gitlab.com/elegant-roots/cadaster